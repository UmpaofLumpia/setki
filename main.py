import numpy
from vtk import vtkUnstructuredGridReader
from vtk.util import numpy_support as VN

reader = vtkUnstructuredGridReader()
reader.SetFileName("C:/Users/Timur/Desktop/cube.vtk")
reader.ReadAllVectorsOn()
reader.ReadAllScalarsOn()
reader.ReadAllTCoordsOn()
reader.Update()

data = reader.GetOutput()
print(data)
